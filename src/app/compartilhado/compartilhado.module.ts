import { NgModule } from "@angular/core";
import {MatTableModule} from '@angular/material/table';
import {MatCardModule} from '@angular/material/card';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatDialogModule} from '@angular/material/dialog';
import { ErrorDialogComponent } from './components/error-dialog/error-dialog.component';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import { CategoriaPipe } from './pipes/categoria.pipe';


@NgModule({

  declarations: [
      ErrorDialogComponent,
      CategoriaPipe
  ],

  imports:[
    MatDialogModule,
    MatButtonModule,
    MatIconModule
  ],

  exports:[
    MatTableModule,
    MatCardModule,
    MatToolbarModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatButtonModule,
    ErrorDialogComponent,
    MatIconModule,
    CategoriaPipe

  ],

})
export class CompartilhadoModule{

}
