import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { delay, first, tap } from 'rxjs/operators';
import { Curso } from '../models/curso';

@Injectable({
  providedIn: 'root'
})
export class CursosService {


  private API = 'api/cursos';

  constructor(private http: HttpClient) { }

  findAll() {
    return this.http.get<Curso[]>(`${this.API}`)
      .pipe(
        first(),
        //delay(15000),
        tap(courses => console.log(courses))
      );
  }

}
