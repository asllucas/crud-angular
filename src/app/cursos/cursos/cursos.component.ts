import { ErrorDialogComponent } from './../../compartilhado/components/error-dialog/error-dialog.component';
import { CursosService } from './../services/cursos.service';
import { Curso } from './../models/curso';
import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-cursos',
  templateUrl: './cursos.component.html',
  styleUrls: ['./cursos.component.scss']
})
export class CursosComponent implements OnInit {

  cursos$: Observable<Curso[]>
  displayedColumns: string[] = ['nome', 'categoria'];

  constructor(private service: CursosService,
    public dialog: MatDialog) {
    this.cursos$ = this.service.findAll()
    .pipe(
      catchError(error => {
       this.onError("Erro ao carregar cursos.")
        return of([])
      })
    )
  }

  ngOnInit(): void {}

  onError(errorMsg: string) {
    this.dialog.open(ErrorDialogComponent, {
      data: errorMsg
    });
  }
}


